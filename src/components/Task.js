import React from "react";

const Task = props => (
    <div className="taskDiv">
        <p className={props.className} ><input type="checkbox"  onChange = {props.change}/>{props.text}</p>
        <button className="btnDelete" onClick = {props.remove}>x</button>
    </div>
);
export default Task;