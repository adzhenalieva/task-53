import React from "react";


const TaskForm = props => (
    <form>
        <p>
            <input className="taskInput" type="text" value = {props.value}  onChange = {props.onChange} />
        </p>
        <button className="btnAdd" onClick = {props.onClick}>Add task</button>
    </form>

);
export default TaskForm;