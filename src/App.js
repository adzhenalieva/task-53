import React, {Component} from 'react';
import TaskForm from './components/AddTaskForm'
import Task from './components/Task'
import './App.css';

class App extends Component {
    state = {
        value: "Add new task",
        tasks: [
            {text: 'Make homework #53'},
            {text: 'Learn new rules'},
            {text: 'Prepare to control work'},
        ]
    };


    changeTask = (event) => {
        this.setState({value: event.target.value});
    };

    addTask = (event) => {
        if(this.state.value !== " " & this.state.value !== "Add new task") {
            const tasksArray = [...this.state.tasks];
            event.preventDefault();

            let newTask = {};
            newTask.text = this.state.value;
            tasksArray.push(newTask);
            console.log(tasksArray);

            this.setState({value: ' ', tasks: tasksArray})
        } else {
            event.preventDefault();
            alert("Please, enter a task")
        }
    };
    removeTask = (id) => {
        const allTasks = [...this.state.tasks];
        allTasks.splice(id, 1);
        this.setState({tasks: allTasks});
    };

    checkTask = (id) => {
        const allTasks = [...this.state.tasks];
        const task = {...allTasks[id]};
        task.classname = 'checked';
        allTasks[id] = task;
        console.log(task.text + ' checked');
        this.setState({tasks: allTasks});
    };

    render() {
        let tasks = this.state.tasks.map((task, id) => (
            <Task
                key = {id}
                text = {task.text}
                remove = {() => this.removeTask(id)}
                className = {task.classname}
                change = {() => this.checkTask(id)}
            >
            </Task>
        ));
        return (
            <div className="App">
                <TaskForm value = {this.state.value}
                          onChange = {event => this.changeTask(event)}
                          onClick = {event => this.addTask(event)}
                >
                </TaskForm>
                {tasks}
            </div>
        );
    }
}

export default App;